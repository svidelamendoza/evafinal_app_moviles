package com.example.eva01_v20;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eva01_v20.data.dao.EvaluacionDao;
import com.example.eva01_v20.data.dao.UsuarioDao;
import com.example.eva01_v20.data.entidades.Evaluacion;
import com.example.eva01_v20.data.entidades.Usuario;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.Calendar;

public class MainActivity_RegisterEV extends AppCompatActivity implements CalendarView.OnDateChangeListener {

    TextView imc;
    EditText pesoActual;
    CalendarView calendario;
    String correo;
    Usuario usuario;
    double estatura;
    double peso;
    double imcCalculado;
    String fecha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main__register_e_v);

        imc = findViewById(R.id.txtactualMC);
        pesoActual = findViewById(R.id.txtactualweight);
        calendario = findViewById(R.id.calendarView);

        calendario.setOnDateChangeListener(this);

        correo = getIntent().getExtras().getString("correo");

        obtenerUsuario(correo);


    }

    private void obtenerUsuario(String correo) {
        UsuarioDao usuarioDao = new UsuarioDao(getApplicationContext());
        usuario = usuarioDao.getUsuarioPorEmail(getApplicationContext(), correo);

        if (!(usuario == null)) {
            estatura = usuario.getEstatura();
        } else {
            Toast.makeText(this, "Instancia de usuario no existe", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSelectedDayChange(@NonNull CalendarView calendarView, int year, int month, int day) {
       /* Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, day);
        */
       month+=1;
        fecha = String.valueOf(year).concat("/").concat(String.valueOf(month)).concat("/").concat(String.valueOf(day));

    }
    public void calculo(View view){
        calcularImc();
    }
    public void calcularImc() {
        double estatura = usuario.getEstatura();
        peso = Double.parseDouble(pesoActual.getText().toString().trim());
        DecimalFormat decimalFormat = new DecimalFormat("#.#");
        imcCalculado = (peso/Math.pow(estatura,2));
        String imcFinal = decimalFormat.format(imcCalculado);
        imc.setText(imcFinal);
    }

    public void registro(){
        calcularImc();

        EvaluacionDao evaluacionDao = new  EvaluacionDao(getApplicationContext());
        Evaluacion evaluacion = new Evaluacion();
        evaluacion.setPeso((int) peso);
        evaluacion.setImc(imcCalculado);
        evaluacion.setFecha(fecha);
        evaluacion.setId_usuario(usuario.getId());

        if(evaluacionDao.insertarEvaluacion(evaluacion)){

            Toast.makeText(this, "Evaluación registrada", Toast.LENGTH_SHORT).show();

            Intent btnguardar = new Intent(this, MainActivity_View.class);
            btnguardar.putExtra("id",usuario.getId());
            startActivity(btnguardar);

        }else{
            Toast.makeText(this, "Problemas al crear el registro", Toast.LENGTH_SHORT).show();
        }
    }

    //Método para botón guardar
    public void Btnguardar(View view) {

        registro();
    }


}
