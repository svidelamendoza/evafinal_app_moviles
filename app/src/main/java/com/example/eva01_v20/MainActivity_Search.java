package com.example.eva01_v20;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.DateFormat;
import java.util.Calendar;

public class MainActivity_Search extends AppCompatActivity implements View.OnClickListener {

    EditText fechaDesde;
    EditText fechaHasta;
    DatePickerDialog.OnDateSetListener fechaDesdeListener;
    DatePickerDialog.OnDateSetListener fechaHastaListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main__search);

        fechaDesde = findViewById(R.id.fechaDesde);
        fechaHasta = findViewById(R.id.fechaHasta);

        fechaDesde.setOnClickListener(this);
        fechaHasta.setOnClickListener(this);

        fechaDesdeListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month+=1;
                fechaDesde.setText(day+"/"+month+"/"+year);
            }
        };

        fechaHastaListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month+=1;
                fechaHasta.setText(day+"/"+month+"/"+year);
            }
        };
    }

    //Método para ir a ver todas las evaluaciones
    public void Ver (View view){
        Intent ver = new Intent(this, MainActivity_View.class);
        startActivity(ver);
    }

    public void buscar(View view){
        Intent buscar = new Intent(this,MainActivity_View.class);
        startActivity(buscar);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fechaDesde:
                Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int dayOfMonth = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog  datePickerDialog = new DatePickerDialog(this,
                        fechaDesdeListener,
                        year,month,dayOfMonth);
                datePickerDialog.show();
                break;
            case R.id.fechaHasta:
                Calendar calendar = Calendar.getInstance();
                int yearHasta = calendar.get(Calendar.YEAR);
                int monthHasta = calendar.get(Calendar.MONTH);
                int dayOfMonthHasta = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog  datePickerDialogHasta = new DatePickerDialog(this,
                        fechaHastaListener,
                        yearHasta,monthHasta,dayOfMonthHasta);
                datePickerDialogHasta.show();
                break;
        }
    }

}